import ciw
import sys
import random
import numpy
import argparse
from datetime import datetime

class BetaDistribution(ciw.dists.Distribution):
	def __init__(self, u, v):
		self.u = u
		self.v = v
	def sample(self):
		ret_value = numpy.random.beta(self.u, self.v)
		return ret_value
		
class NormalDistribution(ciw.dists.Distribution):
	def __init__(self, mid_val, stdev):
		self.mid_val = mid_val
		self.stdev = stdev
	def sample(self):
		ret_value = numpy.random.normal(self.mid_val, self.stdev)
		return ret_value

class ReducedDistribution(ciw.dists.Distribution):
	def __init__(self, distrib, prob=1, a=1, b=1):
		self.prob = prob
		self.a = a
		self.b = b
		if distrib == (ciw.dists.Exponential or ciw.dists.Deterministic):
			self.distrib = distrib(self.a)
		else:
			self.distrib = distrib(self.a, self.b)
	def sample(self):
		ret_value = 0
		if self.prob > 0:
			ret_value = self.distrib.sample()*self.prob
		else:
			ret_value = self.distrib.sample()
		return ret_value

def generate_values_from_distr(values_quantity, distr, prob):
	ret_array = []
	label = 0
	while label <= values_quantity:
		rand_val = random.random()
		rand_buf = 0
		for ix in range(len(prob)):	
			rand_buf = rand_buf + prob[ix]
			if rand_val <= rand_buf:
				ret_array.append(distr[ix].sample())
				break
		label = label + 1
	return ret_array

def createParser ():
	parser = argparse.ArgumentParser()
	parser.add_argument ('--src', default='coap')
	parser.add_argument ('--dst', default='mqtt')
	parser.add_argument ('--mval', default=1)
	parser.add_argument ('--stdev', default=0.1)
	parser.add_argument ('--stime', default=100)
	parser.add_argument ('--mul', default=1)
    
	return parser

src_protocol = ""
dst_protocol = ""
mid_value = 0
standard_dev = 0
simulation_time = 0
mul = 0

if __name__ == '__main__':
	parser = createParser()
	namespace = parser.parse_args()
	label = 0
	print("--------------------------------------------")
	if (namespace.src == "coap" or namespace.src == "mqtt" or namespace.src == "modbus" or namespace.src == "stomp" or namespace.src == "opcua" or namespace.src == "http"):
		src_protocol = namespace.src
	else:
		print("Error in src_protocol variable init! Please input \"--src <protocol>\"\n\twhere <protocol>=\"coap\"\\\"mqtt\"\\\"modbus\"\\\"stomp\"\\\"opcua\"\\\"http\"")
		label = 1
	if (namespace.dst == "coap" or namespace.dst == "mqtt" or namespace.dst == "modbus" or namespace.dst == "stomp" or namespace.dst == "opcua" or namespace.dst == "http"):
		dst_protocol = namespace.dst
	else:
		print("Error in dst_protocol variable init! Please input \"--dst <protocol>\"\n\twhere <protocol>=\"coap\"\\\"mqtt\"\\\"modbus\"\"stomp\"\\\"opcua\"\\\"http\"")
		label = 1
	if float(namespace.mval) > 0:
		mid_value = float(namespace.mval)
	else:
		print("Error in mid_value variable init! Please input \"--mval <value>\"\n\twhere <value> > 0")
		label = 1
	if float(namespace.stdev) > 0:	
		standard_dev = float(namespace.stdev)
	else:
		print("Error in standart_dev variable init! Please input \"--stdev <value>\"\n\twhere <value> > 0")
		label = 1
	if float(namespace.stime) > 0:	
		simulation_time = float(namespace.stime)
	else:
		print("Error in simulation_time variable init! Please input \"--stime <value>\"\n\twhere <value> > 0")
		label = 1
	if int(namespace.mul) > 0:	
		mul = int(namespace.mul)
	else:
		print("Error in mul variable init! Please input \"--mul <value>\"\n\twhere <value> > 0")
		label = 1
	print("--------------------------------------------")
	if label == 0:
		print("Everything is OK!\n")
	else:
		print("There is some errors!\n")
	

if src_protocol == "" or dst_protocol == "" or mid_value <= 0 or standard_dev <= 0 or simulation_time <= 0:
	print("Error in input simulation arguments! Please input next simulation arguments:\n\
	\t\"--src <protocol>\"\n\
	\t\twhere <protocol>=\"coap\"\\\"mqtt\"\\\"modbus\"\"stomp\"\\\"opcua\"\\\"http\"\n\
	\t\"--dst <protocol>\"\n\
	\t\twhere <protocol>=\"coap\"\\\"mqtt\"\\\"modbus\"\"stomp\"\\\"opcua\"\\\"http\"\n\
	\t\"--mval <value>\"\n\
	\t\twhere <value> > 0\n\
	\t\"--stdev <value>\"\n\
	\t\twhere <value> > 0\n\
	\t\"--stime <value>\"\n\
	\t\twhere <value> > 0\n\
	\t\"--mul <value>\"\n\
	\t\twhere <value> > 0\n")
	exit()

service_fname = "service_stat"
arrival_fname = "arrival_stat"

arrival_distrs = []
service_distrs = []

service_prob_arr = []

arrival_distrs.append(ReducedDistribution(NormalDistribution, 1.0, mid_value, standard_dev))

if src_protocol == "coap":
	service_distrs.append(ReducedDistribution(ciw.dists.Gamma, 1.0, 32.42, 0.0000032))
elif src_protocol == "mqtt":
	service_distrs.append(ReducedDistribution(ciw.dists.Exponential, 1.0, 2273.56))
elif src_protocol == "modbus":
	service_distrs.append(ReducedDistribution(ciw.dists.Gamma, 1.0, 43.06, 0.00000098))
elif src_protocol == "stomp":
	service_distrs.append(ReducedDistribution(ciw.dists.Exponential, 1.0, 9096.67))
elif src_protocol == "opcua":
	service_distrs.append(ReducedDistribution(ciw.dists.Gamma, 1.0, 3.0, 0.000067))
elif src_protocol == "http":
	service_distrs.append(ReducedDistribution(ciw.dists.Exponential, 1.0, 6503.96))
else:
	service_distrs.append(ReducedDistribution(ciw.dists.Deterministic, 1.0, 0.001))

if dst_protocol == "coap":
	service_distrs.append(ReducedDistribution(ciw.dists.Gamma, 1.0, 193.68, 0.0000006))
elif dst_protocol == "mqtt":
	service_distrs.append(ReducedDistribution(ciw.dists.Exponential, 1.0, 2414.62))
elif dst_protocol == "modbus":
	service_distrs.append(ReducedDistribution(ciw.dists.Gamma, 1.0, 36.72, 0.0000007))
elif dst_protocol == "stomp":
	service_distrs.append(ReducedDistribution(ciw.dists.Exponential, 1.0, 110220.83))
elif dst_protocol == "opcua":
	service_distrs.append(ReducedDistribution(ciw.dists.Gamma, 0.73, 11.70, 0.0000055))
	service_prob_arr.append(0.73)
	service_distrs.append(ReducedDistribution(ciw.dists.Exponential, 0.27, 94305.55))
	service_prob_arr.append(0.27)
elif dst_protocol == "http":
	service_distrs.append(ReducedDistribution(ciw.dists.Exponential, 1.0, 33806.95))
else:
	service_distrs.append(ReducedDistribution(ciw.dists.Deterministic, 1.0, 0.001))

st_time = datetime.now()

arrival_values = generate_values_from_distr(32768, arrival_distrs, [1.0])
arrival_values = arrival_values*mul
final_arrival_distr = ciw.dists.Sequential(arrival_values)

service_values = []

if len(service_distrs) <= 2:
	service_values1 = generate_values_from_distr(32768, [service_distrs[0]], [1.0])
	service_values2 = generate_values_from_distr(32768, [service_distrs[1]], [1.0])
	service_values = service_values1 + service_values2
elif len(service_distrs) > 2:
	service_values1 = generate_values_from_distr(32768, [service_distrs[0]], [1.0])
	service_values2 = generate_values_from_distr(32768, [service_distrs[1]], [service_prob_arr[0]])
	service_values3 = generate_values_from_distr(32768, [service_distrs[2]], [service_prob_arr[1]])
	service_values = service_values1 + (service_values2 + service_values3)

service_values = service_values*mul
final_service_distr = ciw.dists.Sequential(service_values)

print("generation time:,", datetime.now() - st_time)
st_time = datetime.now()

N = ciw.create_network(
	arrival_distributions=[final_arrival_distr],
	service_distributions=[final_service_distr],
	queue_capacities=['Inf'],
	number_of_servers=[1]	
)

ciw.seed(1)

simulation_time = simulation_time*mul

Q = ciw.Simulation(N)
Q.simulate_until_max_time(simulation_time)
print("simulation time:,", datetime.now() - st_time)

recs = Q.get_all_records()
#waits = [r.waiting_time for r in recs]

service_f = open('results/%s.csv' % service_fname, 'w')
print("no, arrival_intervals, service_time, wait_time, arrival_queue, departure_queue", file=service_f, end="\n")

count = 1
while count < len(recs):
	if count > 1:
		arrival_interval = recs[count].arrival_date - recs[count-1].arrival_date
	else:
		arrival_interval = recs[count].arrival_date
	if arrival_interval < 0:
		arrival_interval = 0
	print("%s, %s, %s, %s, %s, %s" % (count, arrival_interval, recs[count].service_time, recs[count].waiting_time, recs[count].queue_size_at_arrival, recs[count].queue_size_at_departure), file=service_f, end="\n")
	count = count+1
service_f.close()
