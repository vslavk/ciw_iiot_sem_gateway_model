#*Имитационная модель работы семантического шлюза Промышленного Интернета Вещей*
---
Имитационная модель разработана, с помощью языка Python и библиотеки имитационного моделирования ciw.
---
Параметры запуска имитационной модели:
- --src <protocol> where <protocol>=coap\mqtt\modbus\stomp\opcua\http
- --dst <protocol> where <protocol>=coap\mqtt\modbus\stomp\opcua\http
- --mval <value> where <value> > 0
- --stdev <value> where <value> > 0
- --stime <value> where <value> > 0
- --mul <value> where <value> > 0
---
Для написания данного ПО были использованы следующие сторонние библиотеки:  
- Ciw (https://github.com/CiwPython/Ciw);  
- NumPy (https://github.com/numpy/numpy).  
